import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FuncFormatter
from matplotlib import font_manager as fm
import os
import a_SML_WE_SD_Final as a

if __name__ == '__main__':
    
    variables = a.inputs()
    globals().update(variables)
    
    c_, s_, r_, font_path = 1/3, 1/3, 1/3, os.getcwd()
    
    

def Result_Gen(N, RO_switch_, Well_pump_switch_, Gen_switch_init, Cistern_pump_switch_init, Battery_SOC_init, Pressure_Tank_init, Cistern_Tank_init, H_Well_init, c_, s_, r_, font_path=os.getcwd(),n_DIS_FVI = a.n_DIS_FVI):
    ###############################################################################
    #3. Run the model based on optimal control sequence - Discounted 2022
    ###############################################################################
    #Run the model based on optimal control sequence in 2022
    # Path to the directory where your font files are located
    
    # Initial values:
    Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well = Gen_switch_init, Cistern_pump_switch_init, Battery_SOC_init, Pressure_Tank_init, Cistern_Tank_init, H_Well_init
    
    # Add the font to the font manager
    font_files = fm.findSystemFonts(fontpaths=font_path)
    for font_file in font_files:
        fm.fontManager.addfont(font_file)
    
    RO_switch = RO_switch_
    Well_pump_switch = Well_pump_switch_
    
    print("Running the model-Optimal Solution on 2022 data:")
    
    Results_RL_DIS = a.SML(N, RO_switch, Well_pump_switch,    a.WDemand_v, a.EDemand_v, a.GHI_W_m2_v, a.Ambient_temp_v, a.Wind_spd_IOSN3_v, a.R_v,    Power_Table=a.Power_Table,    Cistern_pump_switch = Cistern_pump_switch, Cistern_Tank = Cistern_Tank, Pressure_Tank = Pressure_Tank, Battery_SOC = Battery_SOC, Gen_switch = Gen_switch, H_Well = H_Well,     Battery_total_cap_kWh = a.Battery_total_cap_kWh, init_day = a.init_day, init_h = a.init_h)
    
    
    Results_RL_DIS.to_csv(f"Results_Discounted_FVI_n={n_DIS_FVI}_2022.csv", index=False)###Save the FVI value function:
    
    # Create the first plot
    fig1, ax1 = plt.subplots()
    pd.Series(Results_RL_DIS.loc[:,'Cistern_Tank']).plot(ax=ax1)
    ax1.set_title('Cistern Tank_Discounted FVI')
    ax1.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('1_Cistern_RL2022.png', dpi=300)
    
    # Create the second plot
    fig2, ax2 = plt.subplots()
    pd.Series(Results_RL_DIS.loc[:,'Pressure_Tank']).plot(ax=ax2)
    ax2.set_title('Pressure Tank_Discounted FVI')
    ax2.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('2_PTank_RL2022.png', dpi=300)
    
    # Create the third plot
    fig3, ax3 = plt.subplots()
    pd.Series(Results_RL_DIS.loc[:,'SOC_c']).plot(ax=ax3)
    ax3.set_title('Battery State of Charge_Discounted FVI')
    ax3.set_ylim(bottom=65)
    
    # Save the plot with highest resolution
    plt.savefig('3_SOC_RL2022.png', dpi=300)
    
    ###############################################################################
    #4. Run the model based on Status Quo 2022
    ###############################################################################
    # Run the program for the entire 92 days in munite time step
    
    
    ###Define all inputs from data
    print("Running the model-Status Quo 2022:")
    RO_switch = a.Data22['RO_Switch']
    Well_pump_switch = a.Data22['Well_Switch']
    
    Results_ORG = a.SML(N, RO_switch_v=RO_switch, Well_pump_switch_v=Well_pump_switch, WDemand_v = a.WDemand_v, EDemand_v = a.EDemand_v, GHI_W_m2_v = a.GHI_W_m2_v, Ambient_temp_v = a.Ambient_temp_v, Wind_spd_IOSN3_v = a.Wind_spd_IOSN3_v, R_v = a.R_v)
    
    
    Results_ORG.to_csv('Results_SQ_2022.csv', index=False)
    
    # Create the first plot
    fig1, ax1 = plt.subplots()
    pd.Series(Results_ORG.loc[:,'Cistern_Tank']).plot(ax=ax1)
    ax1.set_title('Cistern Tank SQ')
    ax1.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('4_Cistern_SQ2022.png', dpi=300)
    
    # Create the second plot
    fig2, ax2 = plt.subplots()
    pd.Series(Results_ORG.loc[:,'Pressure_Tank']).plot(ax=ax2)
    ax2.set_title('Pressure Tank SQ')
    ax2.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('5_PTank_SQ2022.png', dpi=300)
    
    # Create the third plot
    fig3, ax3 = plt.subplots()
    pd.Series(Results_ORG.loc[:,'SOC_c']).plot(ax=ax3)
    ax3.set_title('Battery State of Charge SQ')
    ax3.set_ylim(bottom=65)
    
    # Save the plot with highest resolution
    plt.savefig('6_SOC_SQ2022.png', dpi=300)
    
    ###############################################################################
    #5. Run the model based on a heuristic policy that fully utilizes excess energy 2022
    ###############################################################################
    # Run the program for the entire 92 days in munite time step
    #Initial States:
    Gen_switch = Gen_switch_init
    Cistern_pump_switch = Cistern_pump_switch_init
    Battery_SOC = Battery_SOC_init
    Pressure_Tank = Pressure_Tank_init
    Cistern_Tank = Cistern_Tank_init
    H_Well = H_Well_init
    
    print("Running the model-Heuristic policy 2022:")
    
    Results_HEU = a.SML_HU(N, WDemand_v = a.WDemand_v, EDemand_v = a.EDemand_v, GHI_W_m2_v = a.GHI_W_m2_v, Ambient_temp_v = a.Ambient_temp_v, Wind_spd_IOSN3_v = a.Wind_spd_IOSN3_v, R_v = a.R_v)
    
    Results_HEU.to_csv('Results_Heuristic_2022.csv', index=False)
    
    # Create the first plot
    fig1, ax1 = plt.subplots()
    pd.Series(Results_HEU.loc[:,'Cistern_Tank']).plot(ax=ax1)
    ax1.set_title('Cistern Tank HU')
    ax1.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('7_Cistern_HU2022.png', dpi=300)
    
    # Create the second plot
    fig2, ax2 = plt.subplots()
    pd.Series(Results_HEU.loc[:,'Pressure_Tank']).plot(ax=ax2)
    ax2.set_title('Pressure Tank HU')
    ax2.set_ylim(bottom=0)
    
    # Save the plot with highest resolution
    plt.savefig('8_PTank_HU2022.png', dpi=300)
    
    # Create the third plot
    fig3, ax3 = plt.subplots()
    pd.Series(Results_HEU.loc[:,'SOC_c']).plot(ax=ax3)
    ax3.set_title('Battery State of Charge HU')
    ax3.set_ylim(bottom=65)
    
    # Save the plot with highest resolution
    plt.savefig('9_SOC_HU2022.png', dpi=300)
    
    
    ###############################################################################
    # All three cistern tanks in one graph:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot diesel electricity generation
    axs.plot(Results_ORG['Date'], Results_ORG['Cistern_Tank'], label='Status Quo - Cistern Tank Level')
    axs.plot(Results_HEU['Date'], Results_HEU['Cistern_Tank'], label='Heuristic Policy - Cistern Tank Level')
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['Cistern_Tank'], label='Optimal Results - Cistern Tank Level')
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16)
    
    # Set title
    axs.set_title('Comparison of Cistern Tank Levels Across Three Scenarios', fontweight='bold', fontsize=20)
    
    # Add legend
    axs.legend(loc='lower left')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to the y-axis ticks
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Cistern_Tank_Comparison.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    # All three pressurized tanks in one graph:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot diesel electricity generation
    axs.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Status Quo - Pressurized Level')
    axs.plot(Results_HEU['Date'], Results_HEU['Pressure_Tank'], label='Heuristic Policy - Pressurized Tank Level')
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['Pressure_Tank'], label='Optimal Results - Pressurized Tank Level')
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16)
    
    # Set title
    axs.set_title('Comparison of Pressurized Tank Levels Across Three Scenarios', fontweight='bold', fontsize=20)
    
    # Add legend
    axs.legend(loc='lower left')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to the y-axis ticks
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Pressurized_Tank_Comparison.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    # All three battery SOC in one graph:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot diesel electricity generation
    axs.plot(Results_ORG['Date'], Results_ORG['SOC_c'], label='Status Quo - Battery State of Charge')
    axs.plot(Results_HEU['Date'], Results_HEU['SOC_c'], label='Heuristic Policy - Battery State of Charge')
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['SOC_c'], label='Optimal Policy - Battery State of Charge')
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16)
    
    # Set title
    axs.set_title('Comparison of Battery State of Charge Across Three Scenarios', fontweight='bold', fontsize=20)
    
    # Add legend
    axs.legend(loc='lower left')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to the y-axis ticks
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_SOC_Comparison.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    #6. Results graphs 2022
    ###############################################################################
    ## Scores
    #%matplotlib Inline
    
    Title = ['ost','ustainability','eliability']
    i=0
    ## Scores
    # Create subplots
    fig, axs = plt.subplots(3, 1, figsize=(10, 15))
    
    # Plot each time series on its respective subplot
    for ax, score_type in zip(axs, ['C', 'S', 'R']):
        ax.plot(Results_ORG['Date'], Results_ORG[f'Score_{score_type}'], label='Status Quo')
        ax.plot(Results_HEU['Date'], Results_HEU[f'Score_{score_type}'], label='Heuroistic Policy')
        ax.plot(Results_RL_DIS['Date'], Results_RL_DIS[f'Score_{score_type}'], label='Optimized Policy')
    
        # Set x-axis label and tick formatting
        ax.set_xlabel('Date', fontweight='bold', fontsize=16)
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=10*24))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=45)
        
        # Set y-axis label
        ax.set_ylabel(f'{score_type.capitalize()} Score', fontweight='bold', fontsize=16)
    
        # Set title
        ax.set_title(f'{score_type.capitalize()}{Title[i]} Score Results', fontweight='bold', fontsize=20)
    
        # Add legend
        ax.legend(loc='upper right')
    
        # Set font to Arial
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontname('Arial')
        i=i+1
    
    # Adjust layout to prevent overlap
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('10_Scores_2022.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    ## Cumulative Scores
    # Create subplots
    fig, axs = plt.subplots(3, 1, figsize=(10, 15))
    
    i=0
    # Plot each time series on its respective subplot
    for ax, score_type in zip(axs, ['C', 'S', 'R']):
        ax.plot(Results_ORG['Date'], np.cumsum(Results_ORG[f'Score_{score_type}']), label='Status Quo')
        ax.plot(Results_HEU['Date'], np.cumsum(Results_HEU[f'Score_{score_type}']), label='Heuroistic policy')
        ax.plot(Results_RL_DIS['Date'], np.cumsum(Results_RL_DIS[f'Score_{score_type}']), label='Optimal Policy')
    
        # Set x-axis label and tick formatting
        ax.set_xlabel('Date', fontweight='bold', fontsize=16)
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=10*24))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=45)
    
        # Set y-axis label
        ax.set_ylabel(f'Cumulative {score_type.capitalize()} Score', fontweight='bold', fontsize=16)
    
        # Set title
        ax.set_title(f'Cumulative {score_type.capitalize()}{Title[i]} Score Results', fontweight='bold', fontsize=20)
    
        # Add legend
        ax.legend(loc='upper left')
    
        # Set font to Arial
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontname('Arial')
        i=i+1
    
    # Adjust layout to prevent overlap
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('11_Cum_Score_2022.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    ## Diesel Gen and Waste
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots
    fig, axs = plt.subplots(2, 1, figsize=(10, 12))
    
    # Plot diesel electricity generation
    axs[0].plot(Results_ORG['Date'], Results_ORG['Diesel_Gen_kW'], label='Status quo - diesel energy')
    axs[0].plot(Results_HEU['Date'], Results_HEU['Diesel_Gen_kW'], label='Heuroistic - diesel energy')
    axs[0].plot(Results_RL_DIS['Date'], Results_RL_DIS['Diesel_Gen_kW'], label='Discounted FVI - diesel energy')
    
    # Set x-axis label and tick formatting
    axs[0].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[0].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[0].set_ylabel('Energy Load (kW)', fontweight='bold', fontsize=16)
    
    # Set title
    axs[0].set_title('Diesel Electricity Generation', fontweight='bold', fontsize=20)
    
    # Add legend to the first subplot
    axs[0].legend(loc='upper left')
    
    # Plot waste energy utilization
    axs[1].plot(Results_ORG['Date'], Results_ORG['Excess_E_kW'], label='Status Quo - wasted energy')
    axs[1].plot(Results_HEU['Date'], Results_HEU['Excess_E_kW'], label='Heuroistic Policy - wasted energy')
    axs[1].plot(Results_RL_DIS['Date'], Results_RL_DIS['Excess_E_kW'], label='Optimal Policy - wasted energy')
    
    # Set x-axis label and tick formatting
    axs[1].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[1].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[1].set_ylabel('Energy Load (kW)', fontweight='bold', fontsize=16)
    
    # Set title
    axs[1].set_title('Wasted Energy', fontweight='bold', fontsize=20)
    
    # Add legend to the second subplot
    axs[1].legend(loc='upper left')
    
    # Adjust layout to prevent overlap
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('12_Diesel_Waste2022.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    ## Cumulative Diesel Gen and Waste
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots
    fig, axs = plt.subplots(2, 1, figsize=(10, 12))
    
    # Plot diesel electricity generation
    axs[0].plot(Results_ORG['Date'], np.cumsum(Results_ORG['Diesel_Gen_kW']), label='Status Quo - diesel energy')
    axs[0].plot(Results_HEU['Date'], np.cumsum(Results_HEU['Diesel_Gen_kW']), label='Heuroistic Policy - diesel energy')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_RL_DIS['Diesel_Gen_kW']), label='Optimal Policy - diesel energy')
    
    # Set x-axis label and tick formatting
    axs[0].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[0].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[0].set_ylabel('Energy Load (kW)', fontweight='bold', fontsize=16)
    
    # Set title
    axs[0].set_title('Diesel Electricity Generation (Cumulative)', fontweight='bold', fontsize=20)
    
    # Add legend to the first subplot
    axs[0].legend(loc='upper left')
    
    # Plot waste energy utilization
    axs[1].plot(Results_ORG['Date'], np.cumsum(Results_ORG['Excess_E_kW']), label='Status quo - wasted energy')
    axs[1].plot(Results_HEU['Date'], np.cumsum(Results_HEU['Excess_E_kW']), label='Heuroistic - wasted energy')
    axs[1].plot(Results_RL_DIS['Date'], np.cumsum(Results_RL_DIS['Excess_E_kW']), label='Discounted FVI - excess energy')
    
    # Set x-axis label and tick formatting
    axs[1].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[1].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[1].set_ylabel('Energy Load (kW)', fontweight='bold', fontsize=16)
    
    # Set title
    axs[1].set_title('Wasted Energy (Cumulative)', fontweight='bold', fontsize=20)
    
    # Add legend to the second subplot
    axs[1].legend(loc='upper left')
    
    # Adjust layout to prevent overlap
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('13_Cum_Diesel_Waste2022.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    ## RO and Well Utilization
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots
    fig, axs = plt.subplots(2, 1, figsize=(10, 12))
    
    # Plot diesel electricity generation
    axs[0].plot(Results_ORG['Date'], np.cumsum(Results_ORG['RO_Switch']), linestyle='-', color='tab:blue', label='Status quo RO utilization')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_ORG['Well_Switch']), linestyle='--', color='tab:blue', label='Status quo well pump utilization')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_RL_DIS['RO_Switch']), linestyle='-', color='tab:green', label='Optimized RO utilization')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_RL_DIS['Well_Switch']), linestyle='--', color='tab:green', label='Optimized well pump utilization')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_HEU['RO_Switch']), linestyle='-', color='tab:red', label='Heuroistic RO utilization')
    axs[0].plot(Results_RL_DIS['Date'], np.cumsum(Results_HEU['Well_Switch']), linestyle='--', color='tab:red', label='Heuroistic well pump utilization')
    
    # Set x-axis label and tick formatting
    axs[0].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[0].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[0].set_ylabel('Switches Status (minutes)', fontweight='bold', fontsize=16)
    
    # Set title
    axs[0].set_title('Water Preperation Systems Utilization (Cumulative)', fontweight='bold', fontsize=20)
    
    # Add legend to the first subplot
    axs[0].legend(loc='upper left')
    
    # Plot diesel electricity generation
    axs[1].plot(Results_ORG['Date'], (Results_ORG['RO_Switch']),linestyle='-', color='tab:blue', label='Status quo RO utilization')
    axs[1].plot(Results_ORG['Date'], (Results_ORG['Well_Switch']),linestyle='--', color='tab:blue', label='Status quo well pump utilization')
    axs[1].plot(Results_RL_DIS['Date'], (Results_RL_DIS['RO_Switch']), linestyle='-', color='tab:green', label='Optimized RO utilization')
    axs[1].plot(Results_RL_DIS['Date'], (Results_RL_DIS['Well_Switch']), linestyle='--', color='tab:green', label='Optimized well pump utilization')
    axs[1].plot(Results_RL_DIS['Date'], (Results_HEU['RO_Switch']), linestyle='-', color='tab:red', label='Heuroistic RO utilization')
    axs[1].plot(Results_RL_DIS['Date'], (Results_HEU['Well_Switch']), linestyle='-', color='tab:red', label='Heuroistic well pump utilization')
    
    # Set x-axis label and tick formatting
    axs[1].set_xlabel('Date', fontweight='bold', fontsize=16)
    axs[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs[1].xaxis.get_majorticklabels(), rotation=45)
    
    # Set y-axis label
    axs[1].set_ylabel('Switches Status', fontweight='bold', fontsize=16)
    
    # Set title
    axs[1].set_title('Water Preperation Systems Utilization', fontweight='bold', fontsize=20)
    
    # Add legend to the second subplot
    axs[1].legend(loc='upper left')
    
    # Adjust layout to prevent overlap
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('14_Uilization2022.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ###############################################################################
    #7. Results summary 2022
    ###############################################################################
    # Open a file in write mode (this will create a new file or overwrite an existing one)
    with open('Summary2022.txt', 'w') as f:
        # Redirect standard output to the file
        import sys
        sys.stdout = f
    
        # Your prints here
        print("Cost Score:", c_, " Sustainability Score:", s_, " Reliability Score:", r_)
        print("")
        
        
        SQ_Cum_C_Score = round(max(np.cumsum(Results_ORG['Score_C'])),2)
        HEU_Cum_C_Score = round(max(np.cumsum(Results_HEU['Score_C'])),2)
        DIS_Cum_C_Score = round(max(np.cumsum(Results_RL_DIS['Score_C'])),2)
        
        HEU_IMP_Cost = ((HEU_Cum_C_Score-SQ_Cum_C_Score)/(SQ_Cum_C_Score))*100
        DIS_IMP_Cost = ((DIS_Cum_C_Score-SQ_Cum_C_Score)/(SQ_Cum_C_Score))*100
        
        print(f"Heuroistic policy changed the cumulative cost score by {round(HEU_IMP_Cost,1)}% in 92 days")
        print(f"Disounted FVI increased the cumulative cost score by {round(DIS_IMP_Cost,1)}% in 92 days")
        print("")
        
        SQ_Cum_R_Score = round(max(np.cumsum(Results_ORG['Score_R'])),2)
        HEU_Cum_R_Score = round(max(np.cumsum(Results_HEU['Score_R'])),2)
        DIS_Cum_R_Score = round(max(np.cumsum(Results_RL_DIS['Score_R'])),2)
        
        HEU_IMP_Res = ((HEU_Cum_R_Score-SQ_Cum_R_Score)/(SQ_Cum_R_Score))*100
        DIS_IMP_Res = ((DIS_Cum_R_Score-SQ_Cum_R_Score)/(SQ_Cum_R_Score))*100
        
        print(f"Heuroistic policy changed the cumulative reliability score by {round(HEU_IMP_Res,1)}% in 92 days")
        print(f"Disounted FVI increased the reliability cost score by {round(DIS_IMP_Res,1)}% in 92 days")
        print("")
        
        round(Results_RL_DIS['Ground_Water_Depth'].iloc[-1],2)
        
        print(f"However the Height of the well at the end of 92 days in Status Quo is: {round(Results_ORG['Ground_Water_Depth'].iloc[-1],2)} inches")
        print(f"The Height of the well at the end of 92 days in Heuristic Policy is: {round(Results_HEU['Ground_Water_Depth'].iloc[-1],2)} inches")
        print(f"The Height of the well at the end of 92 days in RL Optimized result is: {round(Results_RL_DIS['Ground_Water_Depth'].iloc[-1],2)} inches")
        print("")
        
        SQ_Cum_S_Score = round(max(np.cumsum(Results_ORG['Score_S'])),2)
        HEU_Cum_S_Score = round(max(np.cumsum(Results_HEU['Score_S'])),2)
        DIS_Cum_S_Score = round(max(np.cumsum(Results_RL_DIS['Score_S'])),2)
        
        HEU_IMP_Sus = ((HEU_Cum_S_Score-SQ_Cum_S_Score)/(SQ_Cum_S_Score))*100
        DIS_IMP_Sus = ((DIS_Cum_S_Score-SQ_Cum_S_Score)/(SQ_Cum_S_Score))*100
        
        print(f"Heuroistic policy changed the cumulative sustainability score by {round(HEU_IMP_Sus,1)}% in 92 days")
        print(f"Disounted FVI increased the cumulative sustainability score by {round(DIS_IMP_Sus,1)}% in 92 days")
        print("")
        
        ###############################################################################
        
        SQ_Diesel_kWh = max(np.cumsum(Results_ORG['Diesel_Gen_kW']))/60
        SQ_Excess_kWh = max(np.cumsum(Results_ORG['Excess_E_kW']))/60
        SQ_Excess_min_sum = sum(Results_ORG['Excess_E_Signal'])
        
        RL_HEU_Diesel_kWh = max(np.cumsum(Results_HEU['Diesel_Gen_kW']))/60
        RL_HEU_Excess_kWh = max(np.cumsum(Results_HEU['Excess_E_kW']))/60
        RL_HEU_Excess_min_sum = sum(Results_HEU['Excess_E_Signal'])
        
            
        RL_DIS_Diesel_kWh = max(np.cumsum(Results_RL_DIS['Diesel_Gen_kW']))/60
        RL_DIS_Excess_kWh = max(np.cumsum(Results_RL_DIS['Excess_E_kW']))/60
        RL_DIS_Excess_min_sum = sum(Results_RL_DIS['Excess_E_Signal'])
        
        print("Heuristic Policy Results:")
        print(f"Total diesel electricity generation in 92 days changed from {SQ_Diesel_kWh} to {RL_HEU_Diesel_kWh} kWh which means {(RL_HEU_Diesel_kWh-SQ_Diesel_kWh)*100/SQ_Diesel_kWh}% change.")
        print(f"Total waste energy in 92 days changed from {SQ_Excess_kWh} to {RL_HEU_Excess_kWh} kWh which means {(RL_HEU_Excess_kWh-SQ_Excess_kWh)*100/SQ_Excess_kWh}% change.")
        print(f"Minute-wise excess energy utilization changed from {SQ_Excess_min_sum} to {RL_HEU_Excess_min_sum} mins: {(RL_HEU_Excess_min_sum-SQ_Excess_min_sum)*100/SQ_Excess_min_sum}% change.")
        print()
        print("Discounted FVI Method Results:")
        print(f"Total diesel electricity generation in 92 days changed from {SQ_Diesel_kWh} to {RL_DIS_Diesel_kWh} kWh which means {(RL_DIS_Diesel_kWh-SQ_Diesel_kWh)*100/SQ_Diesel_kWh}% change.")
        print(f"Total waste energy in 92 days changed from {SQ_Excess_kWh} to {RL_DIS_Excess_kWh} kWh which means {(RL_DIS_Excess_kWh-SQ_Excess_kWh)*100/SQ_Excess_kWh}% change.")
        print(f"Minute-wise excess energy utilization changed from {SQ_Excess_min_sum} to {RL_DIS_Excess_min_sum} mins: {(RL_DIS_Excess_min_sum-SQ_Excess_min_sum)*100/SQ_Excess_min_sum}% change.")
        print()
        
        
        SQ_AVG_C_Score = Results_ORG['Score_C'].mean()
        HU_AVG_C_Score = Results_HEU['Score_C'].mean()
        RL_AVG_C_Score = Results_RL_DIS['Score_C'].mean()
        print("Average Cost Score:")
        print(f"SQ: {SQ_AVG_C_Score}")
        print(f"Heuristic: {HU_AVG_C_Score}")
        print(f"RL Optimized: {RL_AVG_C_Score}")
        print()
        
        SQ_AVG_S_Score = Results_ORG['Score_S'].mean()
        HU_AVG_S_Score = Results_HEU['Score_S'].mean()
        RL_AVG_S_Score = Results_RL_DIS['Score_S'].mean()
        print("Average Sustainability Score:")
        print(f"SQ: {SQ_AVG_S_Score}")
        print(f"Heuristic: {HU_AVG_S_Score}")
        print(f"RL Optimized: {RL_AVG_S_Score}")
        print()
        
        SQ_AVG_R_Score = Results_ORG['Score_R'].mean()
        HU_AVG_R_Score = Results_HEU['Score_R'].mean()
        RL_AVG_R_Score = Results_RL_DIS['Score_R'].mean()
        print("Average Reliability Score:")
        print(f"SQ: {SQ_AVG_R_Score}")
        print(f"Heuristic: {HU_AVG_R_Score}")
        print(f"RL Optimized: {RL_AVG_R_Score}")
        print()
        
        SQ_AVG_Total_Score = c_*Results_ORG['Score_C'].mean()+s_*Results_ORG['Score_S'].mean()+r_*Results_ORG['Score_R'].mean()
        HU_AVG_Total_Score = c_*Results_HEU['Score_C'].mean()+s_*Results_HEU['Score_S'].mean()+r_*Results_HEU['Score_R'].mean()
        RL_AVG_Total_Score = c_*Results_RL_DIS['Score_C'].mean()+s_*Results_RL_DIS['Score_S'].mean()+r_*Results_RL_DIS['Score_R'].mean()
        print("Average Total Score:")
        print(f"SQ: {SQ_AVG_Total_Score}")
        print(f"Heuristic: {HU_AVG_Total_Score}")
        print(f"RL Optimized: {RL_AVG_Total_Score}")
        print()
        
        
        print("")
        print("")
        
        RO_ON_SOC_RL = [value for value in Results_RL_DIS['RO_Switch']*Results_RL_DIS['SOC_c'] if value != 0]
        avg_RO_SOC_RL = sum(RO_ON_SOC_RL) / len(RO_ON_SOC_RL)
        print("RL: RO operates on average SOC of ",round(avg_RO_SOC_RL,2),"%")
        
        Well_ON_SOC_RL = [value for value in Results_RL_DIS['Well_Switch']*Results_RL_DIS['SOC_c'] if value != 0]
        avg_Well_SOC_RL = sum(Well_ON_SOC_RL) / len(Well_ON_SOC_RL)
        print("RL: Well operates on average SOC of ",round(avg_Well_SOC_RL,2),"%")
        
        
        print("")
        RO_ON_SOC_SQ = [value for value in Results_ORG['RO_Switch']*Results_ORG['SOC_c'] if value != 0]
        avg_RO_SOC_SQ = sum(RO_ON_SOC_SQ) / len(RO_ON_SOC_SQ)
        print("SQ: RO operates on average SOC of ",round(avg_RO_SOC_SQ,2),"%")
        
        Well_ON_SOC_SQ = [value for value in Results_ORG['Well_Switch']*Results_ORG['SOC_c'] if value != 0]
        avg_Well_SOC_SQ = sum(Well_ON_SOC_SQ) / len(Well_ON_SOC_SQ)
        print("SQ: Well operates on average SOC of ",round(avg_Well_SOC_SQ,2),"%")
        
        print("")
        RO_ON_SOC_HU = [value for value in Results_HEU['RO_Switch']*Results_HEU['SOC_c'] if value != 0]
        avg_RO_SOC_HU = sum(RO_ON_SOC_HU) / len(RO_ON_SOC_HU)
        print("HEU: RO operates on average SOC of ",round(avg_RO_SOC_HU,2),"%")
        
        Well_ON_SOC_HU = [value for value in Results_HEU['Well_Switch']*Results_HEU['SOC_c'] if value != 0]
        avg_Well_SOC_HU = sum(Well_ON_SOC_HU) / len(Well_ON_SOC_HU)
        print("HEU: Well operates on average SOC of ",round(avg_Well_SOC_HU,2),"%")
        
        print("")
        print("")
    
        
        
        # Reset standard output to its default value (usually the console)
        sys.stdout = sys.__stdout__
  
    
    # Cistern tank SQ and water generations in one graph:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_ORG['Date'], Results_ORG['Well_water_flow'], label='Well Water Generation', color='orange', linewidth=0.8)
    axs.plot(Results_ORG['Date'], Results_ORG['RO_water_flow'], label='RO Water Generation', color='green', linewidth=0.8)
    axs.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    axs2.plot(Results_ORG['Date'], Results_ORG['Cistern_Tank'], label='Cistern Tank Level', color='blue')
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Prepared Water (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Cistern Tank Status Over Time', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=19)
    axs.set_yticks(range(0, 19, 2))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=15000)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Water generation and storage.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    
    
    
    
    
    # Pressurized tanks Status quo:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_ORG['Date'], Results_ORG['Water_Demand'], label='Water Demand Outflow', color=(0.2, 0.8, 0.2), linewidth=0.4)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    axs2.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Cistern Tank Level', color='blue')
    axs.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Inflow', color='red', linewidth=1)
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Inflow and Outflow (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Pressurized Tank Status Over Time', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=19)
    axs.set_yticks(range(0, 19, 2))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=4300)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_P_Tank_Balance.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    
    # Cistern tank RL and water generations in one graph:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['Well_water_flow'], label='Well Water Generation', color='orange', linewidth=0.8)
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['RO_water_flow'], label='RO Water Generation', color='green', linewidth=0.8)
    axs.plot(Results_RL_DIS['Date'], Results_RL_DIS['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    axs2.plot(Results_RL_DIS['Date'], Results_RL_DIS['Cistern_Tank'], label='Cistern Tank Level', color='blue')
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Prepared Water (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Stored Water (gal)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Cistern Tank Status Over Time for Optimized System', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=19)
    axs.set_yticks(range(0, 19, 2))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=15000)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Water generation and storage_RL.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    
    
    # Wasted energy SQ:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_ORG['Date'][14*1440-5:30*1440], (Results_ORG['PV_Gen_kW']+Results_ORG['Wind_Gen_kW'])[14*1440-5:30*1440], label='Renewable Power Generation', color='orange', linewidth=0.8)
    axs.plot(Results_ORG['Date'][14*1440-5:30*1440], Results_ORG['Excess_E_kW'][14*1440-5:30*1440], label='Wasted Energy', color='green', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    #axs2.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Cistern Tank Level', color='blue')
    axs2.plot(Results_ORG['Date'][14*1440-5:30*1440], (Results_ORG['RO_water_flow']+Results_ORG['Well_water_flow'])[14*1440-5:30*1440], label='Water Generation', color='blue', linewidth=1)
    #axs2.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Electricity Load (kW)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Water Preparation (gal/min)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Current operation Water Energy Systems Interaction', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=46)
    axs.set_yticks(range(0, 46, 5))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=51)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Energy_Water_SQ.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    
    
    
    
    # Wasted energy RL Solution:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_RL_DIS['Date'][14*1440-5:30*1440], (Results_RL_DIS['PV_Gen_kW']+Results_RL_DIS['Wind_Gen_kW'])[14*1440-5:30*1440], label='Renewable Power Generation', color='orange', linewidth=0.8)
    axs.plot(Results_RL_DIS['Date'][14*1440-5:30*1440], Results_RL_DIS['Excess_E_kW'][14*1440-5:30*1440], label='Wasted Energy', color='green', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    #axs2.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Cistern Tank Level', color='blue')
    axs2.plot(Results_RL_DIS['Date'][14*1440-5:30*1440], (Results_RL_DIS['RO_water_flow']+Results_RL_DIS['Well_water_flow'])[14*1440-5:30*1440], label='Water Generation', color='blue', linewidth=1)
    #axs2.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Electricity Load (kW)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Water Preparation (gal/min)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Optimized Water Energy Systems Interaction', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=46)
    axs.set_yticks(range(0, 46, 5))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=51)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Energy_Water_RL.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    
    
    # Wasted energy HU:
    
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(10, 6))
    
    # Plot RO water generation and well water generation on the right y-axis
    axs.plot(Results_HEU['Date'][14*1440-5:30*1440], (Results_HEU['PV_Gen_kW']+Results_HEU['Wind_Gen_kW'])[14*1440-5:30*1440], label='Renewable Power Generation', color='orange', linewidth=0.8)
    axs.plot(Results_HEU['Date'][14*1440-5:30*1440], Results_HEU['Excess_E_kW'][14*1440-5:30*1440], label='Wasted Energy', color='green', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    #axs2.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Cistern Tank Level', color='blue')
    axs2.plot(Results_HEU['Date'][14*1440-5:30*1440], (Results_HEU['RO_water_flow']+Results_HEU['Well_water_flow'])[14*1440-5:30*1440], label='Water Generation', color='blue', linewidth=1)
    #axs2.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Electricity Load (kW)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Water Preparation (gal/min)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Heuristic Water Energy Systems Interaction', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=46)
    axs.set_yticks(range(0, 46, 5))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=51)
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Energy_Water_HU.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    ####################################### Renewable generation and waste
    from matplotlib.dates import HourLocator, DateFormatter, MinuteLocator
    
    # Create a folder in the home directory
    original_directory = os.getcwd()
    folder_path = os.path.join(os.getcwd(), "Daily_Renewable_E_Gen_Comparison")
    os.makedirs(folder_path, exist_ok=True)
    
    # Change the current working directory to the new folder
    os.chdir(folder_path)
    
    for d in range(0,92):
        #print(d)
        #%matplotlib inline
        # Set font size and style
        plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
        
        # Create subplots with one row and one column
        fig, axs = plt.subplots(1, 1, figsize=(10, 6))
        
        # Total Waste:
        SQ_W = round(sum(Results_ORG['Excess_E_kW'][d*1440:(d+1)*1440])/60)
        HU_W = round(sum(Results_HEU['Excess_E_kW'][d*1440:(d+1)*1440])/60)
        RL_W = round(sum(Results_RL_DIS['Excess_E_kW'][d*1440:(d+1)*1440])/60)
        
        # Plot the data
        axs.plot(Results_HEU['Date'][d*1440:(d+1)*1440], ((Results_HEU['PV_Gen_kW']+Results_HEU['Wind_Gen_kW'])[d*1440:(d+1)*1440]), label='Total Renewable Generation', color='red', linewidth=1)
        
        axs.plot(Results_HEU['Date'][d*1440:(d+1)*1440], ((Results_HEU['PV_Gen_kW']+Results_HEU['Wind_Gen_kW'])[d*1440:(d+1)*1440])-Results_HEU['Excess_E_kW'][d*1440:(d+1)*1440], label='Curtailed Renewable Generation-Heuristic Policy', color='orange', linewidth=0.8)
        axs.plot(Results_ORG['Date'][d*1440:(d+1)*1440], ((Results_ORG['PV_Gen_kW']+Results_ORG['Wind_Gen_kW'])[d*1440:(d+1)*1440])-Results_ORG['Excess_E_kW'][d*1440:(d+1)*1440], label='Curtailed Renewable Generation-Status Quo', color='blue', linewidth=0.8)
        axs.plot(Results_RL_DIS['Date'][d*1440:(d+1)*1440], ((Results_RL_DIS['PV_Gen_kW']+Results_RL_DIS['Wind_Gen_kW'])[d*1440:(d+1)*1440])-Results_RL_DIS['Excess_E_kW'][d*1440:(d+1)*1440], label='Curtailed Renewable Generation-RL Solution', color='green', linewidth=0.8)
        
        # Set x-axis label and tick formatting
        axs.set_xlabel('Time', fontweight='bold', fontsize=16)
        #axs.xaxis.set_major_locator(HourLocator(interval=1))  # Set major locator to HourLocator
        #axs.xaxis.set_minor_locator(MinuteLocator(interval=60))  # Set minor locator to MinuteLocator with a step of 60 minutes
        axs.xaxis.set_major_formatter(DateFormatter('%H:%M'))  # Specify date format
        plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
        
        # Set y-axis label
        axs.set_ylabel('Renewable Generation Load (kW)', fontweight='bold', fontsize=16, color='black')
        
        # Set title
        date_title=Results_HEU['Date'][d*1440].strftime('%b %d, %Y')
        axs.set_title(f'Wasted Renewable Energy Comparison ({date_title})', fontweight='bold', fontsize=20)
        
        # Add legend
        lines1, labels1 = axs.get_legend_handles_labels()
        axs.legend(lines1, labels1, loc='upper right')
        
        # Format y-axis ticks with commas and three digits
        def format_yticks_comma(x, pos):
            return '{:,.0f}'.format(x)
        
        # Apply the function to y-axis
        axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
        
        # Set y-axis limits and ticks
        axs.set_ylim(bottom=0, top=41)
        axs.set_yticks(range(0, 41, 5))
        
        # Write information about total waste on the upper left interior of the graph in a box
        info_text = f'Status Quo Total Waste: {SQ_W} kWh\nHeuristic Policy Total Waste: {HU_W} kWh\nOptimal Policy Total Waste: {RL_W} kWh'
        axs.text(0.02, 0.85, info_text, transform=axs.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.5))
        
        # Save the plot with highest resolution
        plt.savefig(f'Day_{d+1}.png', dpi=300)
        
        # Adjust layout
        plt.tight_layout()
        # Show the plot
        plt.show()
        
    os.chdir(original_directory)
    
    ########## Wasted energy RL Solution vs status quo vs HU with pumps for n days:
    D_i = 30
    D_j = 35
    # Set font size and style
    plt.rcParams.update({'font.size': 12, 'font.family': 'Arial'})
    
    # Create subplots with one row and one column
    fig, axs = plt.subplots(1, 1, figsize=(12, 12))
    
    # Plot the data
    axs.plot(Results_HEU['Date'][D_i*1440:D_j*1440], ((Results_HEU['PV_Gen_kW']+Results_HEU['Wind_Gen_kW'])[D_i*1440:D_j*1440]), label='Total Renewable Generation', color='red', linewidth=1)
    
    axs.plot(Results_HEU['Date'][D_i*1440:D_j*1440], ((Results_HEU['PV_Gen_kW']+Results_HEU['Wind_Gen_kW'])[D_i*1440:D_j*1440])-Results_HEU['Excess_E_kW'][D_i*1440:D_j*1440], label='Curtailed Renewable Generation-Heuristic Policy', color='orange', linewidth=0.8)
    axs.plot(Results_ORG['Date'][D_i*1440:D_j*1440], ((Results_ORG['PV_Gen_kW']+Results_ORG['Wind_Gen_kW'])[D_i*1440:D_j*1440])-Results_ORG['Excess_E_kW'][D_i*1440:D_j*1440], label='Curtailed Renewable Generation-Status Quo', color='blue', linewidth=0.8)
    axs.plot(Results_RL_DIS['Date'][D_i*1440:D_j*1440], ((Results_RL_DIS['PV_Gen_kW']+Results_RL_DIS['Wind_Gen_kW'])[D_i*1440:D_j*1440])-Results_RL_DIS['Excess_E_kW'][D_i*1440:D_j*1440], label='Curtailed Renewable Generation-RL Solution', color='green', linewidth=0.8)
    
    # Create a second y-axis
    axs2 = axs.twinx()
    
    # Plot cistern tank level on the left y-axis
    #axs2.plot(Results_ORG['Date'], Results_ORG['Pressure_Tank'], label='Cistern Tank Level', color='blue')
    axs2.plot(Results_RL_DIS['Date'][D_i*1440:D_j*1440], (Results_RL_DIS['RO_water_flow']+Results_RL_DIS['Well_water_flow'])[D_i*1440:D_j*1440], label='Optimized Water Generation', color='purple', linewidth=0.5)
    #axs2.plot(Results_ORG['Date'], Results_ORG['Water_flow_to_pressure_tank'], label='Pressurized Tank Outflow', color='red', linewidth=0.8)
    
    
    # Set x-axis label and tick formatting
    axs.set_xlabel('Date', fontweight='bold', fontsize=16)
    axs.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    plt.setp(axs.xaxis.get_majorticklabels(), rotation=45)
    
    # Set left y-axis label
    axs.set_ylabel('Electricity Load (kW)', fontweight='bold', fontsize=16, color='black')
    
    # Set right y-axis label
    axs2.set_ylabel('Water Preparation (gal/min)', fontweight='bold', fontsize=16, color='black')
    
    # Set title
    axs.set_title('Optimized Water Energy Systems Interaction', fontweight='bold', fontsize=20)
    
    # Add legend
    lines1, labels1 = axs.get_legend_handles_labels()
    lines2, labels2 = axs2.get_legend_handles_labels()
    axs2.legend(lines1 + lines2, labels1 + labels2, loc='upper right')
    
    # Format y-axis ticks with commas and three digits
    def format_yticks_comma(x, pos):
        return '{:,.0f}'.format(x)
    
    # Apply the function to both y-axes
    axs.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    axs2.yaxis.set_major_formatter(FuncFormatter(format_yticks_comma))
    
    # Set the lower limit of both y-axes to the minimum value
    axs.set_ylim(bottom=0, top=41)
    axs.set_yticks(range(0, 41, 5))
    
    # Set the maximum value for the right y-axis and define tick positions
    axs2.set_ylim(bottom=0, top=61)
    
    # Make a data table
    table_data = np.empty((D_j-D_i, 4), dtype='U20')
    
    for d in range(D_i,D_j):
        #print(m-D_i)
        
        table_data[d-D_i,0] = Results_HEU['Date'][d*1440:d*1440+1].iloc[0].strftime('%b %d %Y')
        table_data[d-D_i,1] = round(sum(Results_ORG['Excess_E_kW'][d*1440:(d+1)*1440])/60)
        table_data[d-D_i,2] = round(sum(Results_HEU['Excess_E_kW'][d*1440:(d+1)*1440])/60)
        table_data[d-D_i,3] = round(sum(Results_RL_DIS['Excess_E_kW'][d*1440:(d+1)*1440])/60)
    
    
    
    # Define column labels
    colLabels = ['Date', 'Status Quo Total Waste\n(kWh)', 'Heuristic Policy Total Waste\n(kWh)', 'Optimal Policy Total Waste\n(kWh)']
    
    # Calculate the number of columns
    num_cols = len(colLabels)
    
    # Create an array to specify the width of each column
    colWidths = [0.8 if i == 0 else 1 for i in range(num_cols)]
    
    # Create the table below the plot
    table = axs.table(cellText=table_data,
                      colLabels=colLabels,
                      loc='bottom',
                      cellLoc='center',
                      colColours=['lightgray'] * len(colLabels),  # Adjust colors based on the number of columns
                      colWidths=colWidths,
                      bbox=[0, -0.55, 1, 0.3])
    
    # Adjust font size and cell padding
    table.auto_set_font_size(False)
    table.set_fontsize(12)
    table.scale(1, 1.5)
    
    # Make the first row of the table bold
    for key, cell in table.get_celld().items():
        if key[0] == 0:
            cell.set_text_props(fontweight='bold')
            cell.set_height(0.055)  # Adjust the height of the first row
    
    # Hide the table axes
    # axs.axis('off')
    
    # Adjust layout
    plt.tight_layout()
    
    # Save the plot with highest resolution
    plt.savefig('0_Energy_Water_RL_Wasted comparision.png', dpi=300)
    
    # Show the plot
    plt.show()
    
    Summary = pd.DataFrame(columns=[''], index=pd.Series(range(0, 2)))
    
    # Define the column names
    columns = [f"{c_} {s_} {r_}","Average cost score", "Average sustainability score", "Average reliability score", "Average total score", "Total diesel electricity generation",	"Total wasted energy", "Avg Treat_$", "Avg Well_$", "Avg RO_$",	"Avg Cistern_$", "Avg Waste_$",	"Avg Diesel_Gen_$",	"Avg Battery_$", "Avg Treat_CF", "Avg Well_CF",	"Avg RO_CF", "Avg Cistern_CF",	"Avg Waste_CF",	"Avg Diesel_Gen_CF", "Avg Battery_CF", "Avg H_Well"]
    
    # Create an empty DataFrame with one empty row
    Summary = pd.DataFrame(columns=columns, index=[0,1,2])
    
    Summary.loc[0] = ["RL", RL_AVG_C_Score, RL_AVG_S_Score, RL_AVG_R_Score, RL_AVG_Total_Score, RL_DIS_Diesel_kWh, RL_DIS_Excess_kWh]+(Results_RL_DIS.iloc[:,36:43].mean()/a.Max_Cost).tolist()+(Results_RL_DIS.iloc[:,28:35].mean()/a.Max_CF).tolist()+[round(Results_RL_DIS['Ground_Water_Depth'].mean(),2)]
    Summary.loc[1] = ["SQ", SQ_AVG_C_Score, SQ_AVG_S_Score, SQ_AVG_R_Score, SQ_AVG_Total_Score, SQ_Diesel_kWh, SQ_Excess_kWh]+(Results_ORG.iloc[:,36:43].mean()/a.Max_Cost).tolist()+(Results_ORG.iloc[:,28:35].mean()/a.Max_CF).tolist()+[round(Results_ORG['Ground_Water_Depth'].mean(),2)]
    Summary.loc[2] = ["HU", HU_AVG_C_Score, HU_AVG_S_Score, HU_AVG_R_Score, HU_AVG_Total_Score, RL_HEU_Diesel_kWh, RL_HEU_Excess_kWh]+(Results_HEU.iloc[:,36:43].mean()/a.Max_Cost).tolist()+(Results_HEU.iloc[:,28:35].mean()/a.Max_CF).tolist()+[round(Results_HEU['Ground_Water_Depth'].mean(),2)]
    
    Summary.to_csv('Summary_2022.csv', index=False)
    
    print(f" Results save in: {os.getcwd()}")
    
    
    return Results_ORG, Results_HEU, Results_RL_DIS, Summary
    
    
if __name__ == '__main__':
    RO_switch_, Well_pump_switch_ = [1] * a.N, [1] * a.N
    Results_ORG, Results_HEU, Results_RL_DIS, Summary = Result_Gen(a.N, RO_switch_, Well_pump_switch_, a.Gen_switch_init, a.Cistern_pump_switch_init, a.Battery_SOC_init, a.Pressure_Tank_init, a.Cistern_Tank_init, a.H_Well_init, c_, s_, r_, font_path)
        
    
    