import os
import multiprocessing
import random
import pandas as pd
import b_RL_NN as b
import c_Result_Gen as c
import torch
import time
import matplotlib.pyplot as plt
from multiprocessing import Pool
from tqdm import tqdm
from joblib import Parallel, delayed

Sens = [[1/3,1/3,1/3],
    [1/2,1/4,1/4],
    [1/4,1/2,1/4],
    [1/4,1/4,1/2],
    [1/2,1/2,0],
    [1/2,0,1/2],
    [0,1/2,1/2],
    [1,0,0],
    [0,1,0],
    [0,0,1]]
# Function to execute a Python file
def execute_file(file_path):
    with open(file_path, 'r') as file:
        exec(file.read(), globals())
        
file_a = "a_SML_WE_SD_Final.py"

execute_file(file_a)

###############################################################################
#Set directory to save results:
###############################################################################
Py_file_directory = os.getcwd()
temp_results_path = os.path.join(os.getcwd(), "Results")

if os.path.exists(temp_results_path) and os.path.isdir(temp_results_path):
    print("Results folder exists. Home directory will be changed to Results")
else:
    # If the folder doesn't exist, create it and then change the current working directory to it
    os.makedirs(temp_results_path)
    print("Results folder is created. Home directory will be changed to Results")

os.chdir(temp_results_path)
base_directory = os.getcwd()

print(f" Results save in: {temp_results_path}")

#Simulation start time in 2022
init_day = 152
init_h = 0
Gen_switch_init = 1
Cistern_pump_switch_init = 0
Battery_SOC_init = 78
Pressure_Tank_init = 4000
Cistern_Tank_init = 14000
H_Well_init = 188.8
###############################################################################
GHI_W_m2_v = Data22.loc[:, 'GHI (W/m2)']
Wind_spd_IOSN3_v = Data22.loc[:, 'WindSpeedIOSN3 (m/s)']
Ambient_temp_v = Data22.loc[:, 'AirTemperature (degrees C)']
EDemand_v = Data22.loc[:, 'Demand (W)']
WDemand_v = Data22.loc[:,'Demand tPlusDifference (gal)']
R_v = Data22.loc[:,'Rain (in)']

#row = Sens[0]
for row in Sens:
    #print(row)
    ## Loading already made samples
    Y=20
    file_path = r'C:\Users\rg1070\OneDrive - USNH\Roozbeh PhD research\Project 7 SML Water-Energy modeling\System Dynamics\SML RL Model\Training Sampled Data'
    file_path = r'C:\Users\USNHIT\OneDrive - USNH\Roozbeh PhD research\Project 7 SML Water-Energy modeling\System Dynamics\SML RL Model\Training Sampled Data'
    
    file_name = f'Training_Data_{int(Y)}Yrs.csv'
    
    # Combine the path and the file name
    full_path = f"{file_path}\\{file_name}"
    
    # Read the CSV file
    #Training_Data = pd.read_csv(file_name)
    
    #print("Data loaded for ", Y," years of data sampling")
    NN_models = []
    
    #Set Result Directory
    # Initialize the directory number
    directory_number = 1

    # Construct the directory path
    directory_path = os.path.join(base_directory, str(directory_number))

    # Check if the directory exists
    while os.path.exists(directory_path):
        # Increment the directory number
        directory_number += 1
        # Construct the new directory path
        directory_path = os.path.join(base_directory, str(directory_number))

    # Create the directory
    os.makedirs(directory_path)
    os.chdir(directory_path)
    os.makedirs(os.path.join(directory_path, str('NN')))
    ###############################################################################
    ###############################################################################
    #Import Data - Majority are already imported in root SML model
    ###############################################################################
    ###############################################################################
    #0. Reward and Transition Functions
    ###############################################################################
    #RL Parameters:
    #Y = 20
    #Number of sampled years
    N = 92*1440  #Steps in 92 days
    n_DIS_FVI = Y*N #Sample size equal to Y years of data!
    d_r = 0.9993
    
    #Model - Reward and transition functions:
    
    ###############################################################################
    # Assigning parameters from each row
    c_ ,s_, r_= row[0], row[1], row[2]
    print("c = ", c_, "s = ", s_, "r = ", r_, " saves at ", directory_path)
    
    def Model(t, RO_switch, Well_pump_switch,   Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,    WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R,   Battery_total_cap_kWh=Battery_total_cap_kWh, Power_Table=Power_Table ,c_= c_, s_= s_, r_= r_, init_day=init_day,init_h=init_h, Lowest_H_Well=Lowest_H_Well, Highest_H_Well=Highest_H_Well):
        Battery_total_cap_kWm = Battery_total_cap_kWh*60
        DC_BUS = Battery_SOC*Battery_total_cap_kWm/100
        
        #Renewable generation
        Civil_time = Time_Day(t,init_h=init_h)
        Day_of_year = Time_Year(t,init_day=init_day)
        PV_gen = PV_Gen(t, GHI_W_m2, Ambient_temp, Day_of_year, Civil_time)
        Wind_gen = Wind_Gen(Wind_spd_IOSN3, Power_Table)
        
        #Total Demand
        RO_water_flow = RO(RO_switch)
        Well_water_flow = Well(Well_pump_switch, H_Well)
        Cistern_pump_switch, Pressure_Tank, Water_flow_to_pressure_tank, Water_shortage = PTank(Pressure_Tank, Cistern_Tank, WDemand, Cistern_pump_switch)
        Cistern_Tank, Excess_water = Cistern(Cistern_Tank, Well_water_flow, RO_water_flow, Water_flow_to_pressure_tank)
        Chlorine_RO, Chlorine_Well, Ch_RO_E_Sig, Ch_Well_E_Sig = Treat(RO_water_flow, Well_water_flow)
        Total_EDemand = E_Demand(t, RO_switch, Well_pump_switch, Water_flow_to_pressure_tank, Ch_RO_E_Sig, Ch_Well_E_Sig, EDemand)
          
        #Watershed model
        H_Well = GroundWater(H_Well, Excess_water, Well_water_flow, R)
        
        #Gen Switch update
        Diesel_electricity_generation, Gen_switch, Excess_gen_production = Diesel_Gen(Gen_switch, DC_BUS, Total_EDemand, PV_gen, Wind_gen)
        
        #SOC update
        DC_BUS, Battery_SOC_c, Excess_energy, Excess_energy_signal = BoSys(DC_BUS, PV_gen, Wind_gen, Diesel_electricity_generation, Total_EDemand, Excess_gen_production, Gen_switch)
        Battery_SOC_c = DC_BUS*100/Battery_total_cap_kWm
        
        #Reward
        Score_C, Diesel_Gen_C, Waste_C, Cistern_C, RO_C, Well_C, Treat_C, Battery_C = CostScore(RO_switch, Well_pump_switch, Gen_switch, Cistern_pump_switch,  Diesel_electricity_generation, Excess_energy, Battery_SOC_c, Battery_SOC)
        Score_S, Diesel_Gen_CO2, Waste_CO2, Cistern_CO2, RO_CO2, Well_CO2, Treat_CO2, Battery_CO2 = SusScore(RO_switch, Well_pump_switch, Gen_switch, Cistern_pump_switch,  Diesel_electricity_generation, Excess_energy, Battery_SOC_c, Battery_SOC)
        Score_R = ReliScore(H_Well)
        
        #r=-Diesel_electricity_generation
        #r=-Excess_energy
        #r=-(Diesel_electricity_generation+Excess_energy)/35
        #if Battery_SOC>=70 and Battery_SOC<=78 and Gen_switch == 0:
        #    r=r+(Battery_SOC_c-Battery_SOC)
        r = (c_ * Score_C) + (s_ * Score_S) + (r_ * Score_R)
        
        #Cistern constrain:
        if Water_shortage > 0:
            r = r-Water_shortage/WDemand # Penalty for not satisfying demand.
        
        #Excess water constrain:
        if H_Well>=Highest_H_Well and Excess_water>0:
            r = r-Excess_water/(RO_switch*3 + Well_pump_switch*14.5)  #Penalty for well over run
            
        return r, Gen_switch, Cistern_pump_switch, Battery_SOC_c, Pressure_Tank, Cistern_Tank, H_Well
    
    ###############################################################################
    #0. Stochasticity functions and sample generation:
    ###############################################################################
    
    def Ex_S_Rand(i,window_size = 30): #Returns one sampled list of exogenous state features for step i
        ##GHI and Temp
        # Randomly pick a row and a column
        random_row_index = i  # time identification
        random_column = np.random.choice([1, 2, 3, 4, 5])  # Random column
        
        # Check if the selected cell is NA
        while pd.isna(His['GHI_Full'].iloc[random_row_index, random_column]) or pd.isna(His['Temp_Full'].iloc[random_row_index, random_column]):
            
            random_column = np.random.choice([1, 2, 3, 4, 5]) # Pick another row index
        
        GHI=His['GHI_Full'].iloc[random_row_index, random_column]
        Temp=His['Temp_Full'].iloc[random_row_index, random_column]
        
        ##Water Demand
        original_list = Data22.loc[:, 'Demand tPlusDifference (gal)']
        
        # Randomly select an index from the list
        random_index = i
    
        # Calculate the start and end index for the window
        if random_row_index < window_size+1:
            start_index = 0
            end_index = random_row_index + window_size
        else:
            start_index = random_row_index - window_size
            end_index = random_row_index + window_size
            if end_index > len(original_list):
                end_index = len(original_list)
        
        # Extract the window of values
        window_values = original_list[start_index:end_index]
        
        # Calculate the mean and standard deviation for the window
        window_mean = np.mean(window_values)
        window_std_dev = np.std(window_values)
        
        #Assign the random value
        WD = np.random.normal(window_mean, window_std_dev)
        if WD<=0:
            WD=0
            
        ##Energy Demand
        original_list = Data22.loc[:, 'Demand (W)']
    
        # Randomly select an index from the list
        random_index = i
    
        # Calculate the start and end index for the window
        if random_row_index < window_size+1:
            start_index = 0
            end_index = random_row_index + window_size
        else:
            start_index = random_row_index - window_size
            end_index = random_row_index + window_size
            if end_index > len(original_list):
                end_index = len(original_list)
    
        # Extract the window of values
        window_values = original_list[start_index:end_index]
    
        # Calculate the mean and standard deviation for the window
        window_mean = np.mean(window_values)
        window_std_dev = np.std(window_values)
    
        #Assign the random value
        ED = (np.random.normal(window_mean, window_std_dev))
        if ED<=0:
            ED=0
        
        ##Wind speed and Precipitation
        # Randomly pick a row and a column
        random_row_index = i
        random_column = np.random.choice([1, 2, 3, 4, 5])  # Random column
    
        # Check if the selected cell is NA
        while pd.isna(His['Wind_Full'].iloc[random_row_index, random_column]):
            
            random_column = np.random.choice([1, 2, 3, 4, 5]) # Pick another row index
    
        #Assign random values
        Wind = (His['Wind_Full'].iloc[random_row_index, random_column])
        R = (Data22.loc[np.random.randint(0, len(Data22)),'Rain (in)'])
    
        return WD, ED, GHI, Temp, Wind, R
    
    def Ex_S_Rand_List_i(n, i): # returns n samples of exogenous state features for time step i
        # Endogenous states
        # Exogenous states
        WDemand = []
        EDemand = []
        GHI_W_m2 = []
        Ambient_temp = []
        Wind_spd_IOSN3 = []
        R = []
        time_step = []
    
        for _ in range(n):
            
            a=Ex_S_Rand(i)
            
            WDemand.append(a[0])
            EDemand.append(a[1])
            GHI_W_m2.append(a[2])
            Ambient_temp.append(a[3])
            Wind_spd_IOSN3.append(a[4])
            R.append(a[5])
            
        
        return WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R
    
    def Ex_S_Rand_List(n): # returns n samples of exogenous state features for equally sampled for time steps
        # Endogenous states
        # Exogenous states
        WDemand = []
        EDemand = []
        GHI_W_m2 = []
        Ambient_temp = []
        Wind_spd_IOSN3 = []
        R = []
        time_step = []
    
        for _ in tqdm(range(n), desc='Random value geneartion for states: ', unit='Steps'):
            # Randomly pick a row and a column
            i = int(_/Y)  # time identification
            
            a=Ex_S_Rand(i)
            
            WDemand.append(a[0])
            EDemand.append(a[1])
            GHI_W_m2.append(a[2])
            Ambient_temp.append(a[3])
            Wind_spd_IOSN3.append(a[4])
            R.append(a[5])
            time_step.append(i)
        
        return WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R, time_step
    
    def End_S_Rand_List(n): # returns n samples of endogenous state features
        # Endogenous states
        Gen_switch = random.choices([0, 1], k=n)
        Cistern_pump_switch = random.choices([0, 1], k=n)
        Battery_SOC = [round(random.uniform(69, 100.0001), 2) for _ in range(n)]
        Pressure_Tank = [round(random.uniform(0, 4000.0001), 2) for _ in range(n)]
        Cistern_Tank = [round(random.uniform(0, 14000.0001), 2) for _ in range(n)]
        H_Well = [round(random.uniform(Lowest_H_Well, Highest_H_Well), 2) for _ in range(n)]
        
        return Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well
    
    ##Parallel randomized exogenousstates:
    
    def generate_random_values(i):
        # Call Ex_S_Rand(i) to obtain randomly generated values for exogenous state features
        exogenous_values = Ex_S_Rand(i)
        
        # Extract individual values from the returned tuple
        WD, ED, GHI, Temp, Wind, R = exogenous_values
        
        return WD, ED, GHI, Temp, Wind, R
    
    def generate_random_values_wrapper(args):
        return generate_random_values(*args)
    
    def Ex_S_Rand_List_Paral(n, Y):
        time_step = []
        inputs = [(int(_/Y),) for _ in range(n)]
        with Pool() as p:
            results = list(tqdm(p.imap(generate_random_values_wrapper, inputs), total=n, desc='Random value generation for states', unit='Steps'))
    
        WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R = zip(*results)
        
        time_step = []
        for _ in range(n):
            time_step.append(int(_/Y))
        return list(WDemand), list(EDemand), list(GHI_W_m2), list(Ambient_temp), list(Wind_spd_IOSN3), list(R), list(time_step)
    ###############################################################################
    #Sample of n_DIS_FVI=N*Y observations for 12 stats
    ###############################################################################
    # Endogenous states
    columns = [f"{c_} {s_} {r_}","Average cost score", "Average sustainability score", "Average reliability score", "Average total score", "Total diesel electricity generation",	"Total wasted energy", "Avg Treat_$", "Avg Well_$", "Avg RO_$",	"Avg Cistern_$", "Avg Waste_$",	"Avg Diesel_Gen_$",	"Avg Battery_$", "Avg Treat_CF", "Avg Well_CF",	"Avg RO_CF", "Avg Cistern_CF",	"Avg Waste_CF",	"Avg Diesel_Gen_CF", "Avg Battery_CF", "Avg H_Well"]
    Summary = pd.DataFrame(columns=columns, index=range(100))
    for Run in range(100):
        
        start_time = time.time()
        Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well = End_S_Rand_List(n_DIS_FVI)
        
        #WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R, time_step = Ex_S_Rand_List(n_DIS_FVI)
        # Parallel
        WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R, time_step = Ex_S_Rand_List_Paral(n_DIS_FVI,Y)
        
        Training_Data = {
            'i': time_step,
            'cat_var1': Gen_switch,
            'cat_var2': Cistern_pump_switch,
            'numerical_var1': Battery_SOC,
            'numerical_var2': Pressure_Tank,
            'numerical_var3': Cistern_Tank,
            'numerical_var4': H_Well,
            'numerical_var5': WDemand,
            'numerical_var6': EDemand,
            'numerical_var7': GHI_W_m2,
            'numerical_var8': Ambient_temp,
            'numerical_var9': Wind_spd_IOSN3,
            'numerical_var10': R,
            'target_variable': ["NA"]*len(R)
        }
        
        ###############################################################################
        # Simplified FVI discounted
        ###############################################################################
        ##########################################################################@####
        #Parallel Simplified discounted DVI
        def Policy_Rollout_Sim(t, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, d_r=d_r):
            ret = []
            for RO_s in (1,0):
                for Well_pump_s in (1,0):
                    
                    r, Gen_switch_N, Cistern_pump_switch_N, Battery_SOC_N, Pressure_Tank_N, Cistern_Tank_N, H_Well_N = Model(t, RO_s, Well_pump_s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12)
                    v = r*(1 - d_r**(N - t)) / (1 - d_r)
                    ret.append(v)
                    
            v= max(ret)
            return v
        
        def Policy_Rollout_batch_Sim(data_batch, d_r):
            Ret_batch = []
            for row in data_batch:
                t, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12 = row
                v = Policy_Rollout_Sim(t, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, d_r=d_r)
                Ret_batch.append(v)
            return Ret_batch
        
        def FVI_DIS_Simp_batch(batch_data, d_r):
            num_cores = multiprocessing.cpu_count()
            num_batches = len(batch_data) // 100 + 1
            
            results = Parallel(n_jobs=num_cores)(
                delayed(Policy_Rollout_batch_Sim)(data_batch, d_r) 
                for data_batch in tqdm(np.array_split(batch_data, num_batches), desc='Progress of sampled batches on V calculation: ', unit='batches')
            )
            
            return np.concatenate(results)
        
        def FVI_DIS_ROLL_Sim(time_step, Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R, d_r=d_r):
            batch_data = np.column_stack((time_step, Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R))
            return FVI_DIS_Simp_batch(batch_data, d_r)
        
        def run_FVI_DIS_ROLL_Sim(Training_Data, d_r=d_r):
            df = pd.DataFrame(Training_Data)#pd.DataFrame(Training_Data)
            time_step, Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R = df[df.columns[0]], df[df.columns[1]], df[
                df.columns[2]], df[df.columns[3]], df[df.columns[4]], df[df.columns[5]], df[df.columns[6]], df[df.columns[7]], df[df.columns[8]], df[df.columns[9]], df[df.columns[10]], df[df.columns[11]], df[df.columns[12]]
        
            
            Ret_S = FVI_DIS_ROLL_Sim(time_step, Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, 
                                 Cistern_Tank, H_Well, WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R, d_r=d_r)
        
            #Ret_S = df[df.columns[13]]
            # Neural network:
            df['target_variable'] = Ret_S
            #df.to_csv('Training_Data.csv', index=False)
        
            # NN Hyper Parameters
            L2_1 = 32
            L2_2 = 64
            L2_3 = 32

            batch_size = 1000000
            num_epochs = 100
        
            Model_DIS_NN, losses, test_loss = b.RL_NN_3L(df, num_epochs=num_epochs,
                                    batch_size=batch_size, L1=L2_1, L2=L2_2, L3=L2_3)
            
            plt.plot(range(1, num_epochs+1), losses, 'ro', markersize=3)
            plt.xlabel('Epoch') 
            plt.ylabel('MSE Loss')
            plt.title(f'MSE Loss vs Epoch - Training.\n Train Loss: {min(losses):.4f} \n Test Loss: {test_loss.item():.4f}')
            plt.grid(True)
            #plt.show()
            
            # Save the plot with highest resolution
            plt.savefig(f'NN/NN_{Run}.png', dpi=300)
            
            return Model_DIS_NN, Training_Data
        
        # Optimal Control Sequence
        ###############################################################################
        def OptContSeq_DIS(Model_DIS_NN, Data=Data22, Gen_switch=Gen_switch_init, Cistern_pump_switch=Cistern_pump_switch_init, Battery_SOC=Battery_SOC_init, Pressure_Tank=Pressure_Tank_init, Cistern_Tank=Cistern_Tank_init, H_Well=H_Well_init):
            RO_switch_ = []
            Well_pump_switch_ = []
        
            #RO_s, Well_pump_s = 1, 1
        
            for t in tqdm(range(0, N-1), desc='Control Sequence: ', unit='Steps'):
                WDemand = Data.loc[t, 'Demand tPlusDifference (gal)']
                EDemand = Data.loc[t, 'Demand (W)']
                GHI_W_m2 = Data.loc[t, 'GHI (W/m2)']
                Ambient_temp = Data.loc[t, 'AirTemperature (degrees C)']
                Wind_spd_IOSN3 = Data.loc[t, 'WindSpeedIOSN3 (m/s)']
                R = Data.loc[t, 'Rain (in)']
        
                result_dict = {}
        
                for RO_s in (1, 0):
                    for Well_pump_s in (1, 0):
        
                        if Cistern_Tank <= 0.1*14000 and Well_pump_s == 0:
                            # This par avoids turning both systems off when cistern tank is empty by excluding {0,0} from action space.
                            None
                        else:
                            r, Gen_switch_N, Cistern_pump_switch_N, Battery_SOC_N, Pressure_Tank_N, Cistern_Tank_N, H_Well_N = Model(
                                t, RO_s, Well_pump_s,   Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R)
        
                            WDemand_N = Data.loc[t+1, 'Demand tPlusDifference (gal)']
                            EDemand_N = Data.loc[t+1, 'Demand (W)']
                            GHI_W_m2_N = Data.loc[t+1, 'GHI (W/m2)']
                            Ambient_temp_N = Data.loc[t+1,
                                                      'AirTemperature (degrees C)']
                            Wind_spd_IOSN3_N = Data.loc[t+1, 'WindSpeedIOSN3 (m/s)']
                            R_N = Data.loc[t+1, 'Rain (in)']
        
                            # NN
                            sample_input = torch.tensor([[Gen_switch_N, Cistern_pump_switch_N, Battery_SOC_N, Pressure_Tank_N, Cistern_Tank_N, H_Well_N,   WDemand_N, EDemand_N,
                                                        GHI_W_m2_N, Ambient_temp_N, Wind_spd_IOSN3_N, R_N]], dtype=torch.float32)  # Input data with 2 numerical variables and 2 categorical variables
                            predicted_output = Model_DIS_NN(sample_input)
                            #print("Predicted output:", predicted_output.item())
                            # *(1 - d_r**(N - t)) / (1 - d_r)
                            result_dict[(RO_s, Well_pump_s)] = r + \
                                predicted_output.item()
        
                values = list(result_dict.values())
                keys = list(result_dict.keys())
        
                max_value = max(values)
                max_value_indices = [i for i, v in enumerate(values) if v == max_value]
        
                if len(max_value_indices) == 1:  # Only one maximum value
                    max_value = max(result_dict.values())
                    corresponding_values = [
                        key for key, value in result_dict.items() if value == max_value]
        
                    RO_switch_.append(corresponding_values[0][0])
                    Well_pump_switch_.append(corresponding_values[0][1])
        
                else:  # Multiple maximum values
                    # Find the key where the sum of values is maximum-We prefer to run the pumps when there is no value added to the rewards, rather than not generating water.
                    sum_values = [sum(keys[i]) for i in max_value_indices]
                    max_sum_index = sum_values.index(max(sum_values))
                    result = keys[max_value_indices[max_sum_index]]
        
                    # This does not let the well pump run when recharging the well!
                    if Cistern_Tank == 14000 and result == (1, 1):
        
                        RO_switch_.append(1)
                        Well_pump_switch_.append(0)
                    else:
        
                        RO_switch_.append(result[0])
                        Well_pump_switch_.append(result[1])
        
                r, Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well = Model(t, RO_switch_[t], Well_pump_switch_[
                                                                                                             t],   Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank, Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R)
               ###############
        
            # Terminal decision
            t = N-1
            WDemand = Data.loc[t, 'Demand tPlusDifference (gal)']
            EDemand = Data.loc[t, 'Demand (W)']
            GHI_W_m2 = Data.loc[t, 'GHI (W/m2)']
            Ambient_temp = Data.loc[t, 'AirTemperature (degrees C)']
            Wind_spd_IOSN3 = Data.loc[t, 'WindSpeedIOSN3 (m/s)']
            R = Data.loc[t, 'Rain (in)']
        
            result_dict = {}
            for RO_s in (1, 0):
                for Well_pump_s in (1, 0):
        
                    r, *_ = Model(t, RO_s, Well_pump_s,   Gen_switch, Cistern_pump_switch, Battery_SOC, Pressure_Tank,
                                  Cistern_Tank, H_Well,   WDemand, EDemand, GHI_W_m2, Ambient_temp, Wind_spd_IOSN3, R)
        
                    result_dict[(RO_s, Well_pump_s)] = r
        
            values = list(result_dict.values())
            keys = list(result_dict.keys())
        
            max_value = max(values)
            max_value_indices = [i for i, v in enumerate(values) if v == max_value]
        
            if len(max_value_indices) == 1:  # Only one maximum value
                max_value = max(result_dict.values())
                corresponding_values = [
                    key for key, value in result_dict.items() if value == max_value]
        
                RO_switch_.append(corresponding_values[0][0])
                Well_pump_switch_.append(corresponding_values[0][1])
        
            else:  # Multiple maximum values
                # Find the key where the sum of values is maximum-We prefer to run the pumps when there is no value added to the rewards, rather than not generating water.
                sum_values = [sum(keys[i]) for i in max_value_indices]
                max_sum_index = sum_values.index(max(sum_values))
                result = keys[max_value_indices[max_sum_index]]
        
                # This does not let the well pump run when recharging the well!
                if Cistern_Tank == 14000 and result == (1, 1):
                    RO_switch_.append(1)
                    Well_pump_switch_.append(0)
                else:
                    RO_switch_.append(result[0])
                    Well_pump_switch_.append(result[1])
        
            return RO_switch_, Well_pump_switch_
        
        ###############################################################################
        # Run the model:
        ###############################################################################
        ###############################################################################
        ## Run Discounted FVI Simplified and optimal control sequence
        d_r = 0.9993
        #Model_DIS_NN, Training_Data = run_FVI_DIS_Sim(d_r=d_r) # Takes 1 hr for 20 years of data
        ##Parallel
        Model_DIS_NN, Training_Data = run_FVI_DIS_ROLL_Sim(Training_Data, d_r=d_r)
        RO_switch_, Well_pump_switch_ = OptContSeq_DIS(Model_DIS_NN, Data=Data22, Gen_switch = Gen_switch_init, Cistern_pump_switch = Cistern_pump_switch_init, Battery_SOC = Battery_SOC_init, Pressure_Tank = Pressure_Tank_init, Cistern_Tank = Cistern_Tank_init, H_Well = H_Well_init)
        
        #Run the model based on optimal control sequence in 2022
        Results_RL_DIS = SML(N, RO_switch_, Well_pump_switch_,    WDemand_v, EDemand_v, GHI_W_m2_v, Ambient_temp_v, Wind_spd_IOSN3_v, R_v,    Power_Table=Power_Table,    Cistern_pump_switch = Cistern_pump_switch_init, Cistern_Tank = Cistern_Tank_init, Pressure_Tank = Pressure_Tank_init, Battery_SOC = Battery_SOC_init, Gen_switch = Gen_switch_init, H_Well = H_Well_init,     Battery_total_cap_kWh = Battery_total_cap_kWh, init_day = init_day,init_h = init_h)
        
        # Create an empty DataFrame with one empty row
        # Define the column names
               
        RL_AVG_C_Score, RL_AVG_S_Score, RL_AVG_R_Score, RL_AVG_Total_Score = Results_RL_DIS['Score_C'].mean(), Results_RL_DIS['Score_S'].mean(), Results_RL_DIS['Score_R'].mean(), c_*Results_RL_DIS['Score_C'].mean()+s_*Results_RL_DIS['Score_S'].mean()+r_*Results_RL_DIS['Score_R'].mean()
        RL_DIS_Diesel_kWh = max(np.cumsum(Results_RL_DIS['Diesel_Gen_kW']))/60
        RL_DIS_Excess_kWh = max(np.cumsum(Results_RL_DIS['Excess_E_kW']))/60
        
        Summary.loc[Run] = [Run, RL_AVG_C_Score, RL_AVG_S_Score, RL_AVG_R_Score, RL_AVG_Total_Score, RL_DIS_Diesel_kWh, RL_DIS_Excess_kWh]+(Results_RL_DIS.iloc[:,36:43].mean()/Max_Cost).tolist()+(Results_RL_DIS.iloc[:,28:35].mean()/Max_CF).tolist()+[round(Results_RL_DIS['Ground_Water_Depth'].mean(),2)]
        
        # Save the NN associated with each run
        NN_models.append(Model_DIS_NN)
        
        hours, rem = divmod(time.time() - start_time, 3600)
        minutes, seconds = divmod(rem, 60)
        print(f"Program runtime: {int(hours):02}:{int(minutes):02}:{seconds:.2f}", ". Run Number: ",Run+1)
        print()
        
    ###############################################################################
    # Extract the results
    ###############################################################################
    Summary.to_csv(f"Summary_{c_} {s_} {r_}.csv", index=False)
    
    # Get the model with max avg_total score and run to get RO/Well switches:
    index = np.argmax(Summary['Average total score'])
    Model_DIS_NN = NN_models[index]
    torch.save(Model_DIS_NN.state_dict(), 'NN_Best_Model.pth')
    
    RO_switch_, Well_pump_switch_ = OptContSeq_DIS(Model_DIS_NN, Data=Data22, Gen_switch = Gen_switch_init, Cistern_pump_switch = Cistern_pump_switch_init, Battery_SOC = Battery_SOC_init, Pressure_Tank = Pressure_Tank_init, Cistern_Tank = Cistern_Tank_init, H_Well = H_Well_init)
    
    Results_ORG, Results_HEU, Results_RL_DIS, Summary = c.Result_Gen(N, RO_switch_, Well_pump_switch_, Gen_switch_init = Gen_switch_init, Cistern_pump_switch_init = Cistern_pump_switch_init, Battery_SOC_init = Battery_SOC_init, Pressure_Tank_init = Pressure_Tank_init, Cistern_Tank_init = Cistern_Tank_init, H_Well_init = H_Well_init, c_=c_, s_=s_, r_=r_, font_path = Py_file_directory)
    
    df = pd.DataFrame(Training_Data)
    df.to_csv(f"Training_Data_{c_} {s_} {r_}.csv", index=False)
    
    #Back to Results Directory
    os.chdir(temp_results_path)

###############################################################################
# End of Sensitivity Analysis
###############################################################################