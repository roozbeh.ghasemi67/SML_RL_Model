import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import torch.multiprocessing as mp

## 3 Layers with TQDM #########################################################
class SimpleNN_3L(nn.Module):
    def __init__(self, input_size, L1, L2, L3):
        super(SimpleNN_3L, self).__init__()
        self.fc1 = nn.Linear(input_size, L1)
        self.fc2 = nn.Linear(L1, L2)
        self.fc3 = nn.Linear(L2, L3)
        self.fc4 = nn.Linear(L3, 1)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = torch.relu(self.fc3(x))
        x = self.fc4(x)
        return x

def train_3L(model, criterion, optimizer, X_train, y_train, batch_size, num_epochs, device):
    model.train()
    losses = []
    best_loss = float('inf')
    best_model_state = None

    for epoch in tqdm(range(num_epochs), desc="Neural Network - Training "):
        epoch_loss = 0.0
        for i in range(0, len(X_train), batch_size):
            inputs = X_train[i:i+batch_size].to(device)
            targets_batch = y_train[i:i+batch_size].to(device)

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, targets_batch)
            loss.backward()
            optimizer.step()

            epoch_loss += loss.item()
        
        avg_epoch_loss = epoch_loss / (len(X_train) // batch_size)
        losses.append(avg_epoch_loss)
        
        # Checkpointing
        if avg_epoch_loss < best_loss:
            best_loss = avg_epoch_loss
            best_model_state = model.state_dict()

    return losses, best_model_state

def RL_NN_3L(Training_Data, num_epochs=100, batch_size=1000, L1=124, L2=256, L3=512, weight_decay=0.01):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")
    
    df_tr = pd.DataFrame(Training_Data)

    numerical_data = torch.tensor(df_tr[['numerical_var1', 'numerical_var2', 'numerical_var3', 'numerical_var4', 'numerical_var5', 'numerical_var6', 'numerical_var7', 'numerical_var8', 'numerical_var9', 'numerical_var10']].values, dtype=torch.float32)
    categorical_data = torch.tensor(df_tr[['cat_var1', 'cat_var2']].values, dtype=torch.float32)
    input_data = torch.cat((numerical_data, categorical_data), dim=1)

    model = SimpleNN_3L(input_data.shape[1], L1, L2, L3).to(device)

    if torch.cuda.device_count() > 1:
        print("Using", torch.cuda.device_count(), "GPUs!")
        model = nn.DataParallel(model)

    model = model.to(device)

    targets = torch.tensor(df_tr['target_variable'].values, dtype=torch.float32).view(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(input_data, targets, test_size=0.2, random_state=42)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001, weight_decay=weight_decay)

    # Set multiprocessing start method
    if __name__ == '__main__':
        if mp.get_start_method(allow_none=True) != 'spawn':
            mp.set_start_method('spawn')
    
    losses, best_model_state = train_3L(model, criterion, optimizer, X_train, y_train, batch_size, num_epochs, device)

    # Restore the best model state
    model.load_state_dict(best_model_state)

    with torch.no_grad():
        model.eval()
        test_outputs = model(X_test.to(device))
        test_loss = criterion(test_outputs, y_test.to(device))
        print(f'Test Loss: {test_loss.item():.4f}')
    
    
    
    
    return model, losses, test_loss

if __name__ == '__main__':
    
    Training_Data = pd.read_csv('Training_Data_20Yrs.csv')
    
    # NN Hyper Parameters
    L2_1 = 32
    L2_2 = 64
    L2_3 = 32

    batch_size = 1000000
    num_epochs = 100

    Model_DIS_NN, losses, test_loss = RL_NN_3L(Training_Data, num_epochs=num_epochs,
                            batch_size=batch_size, L1=L2_1, L2=L2_2, L3=L2_3)
    
    plt.plot(range(1, num_epochs+1), losses, 'ro', markersize=3)
    plt.xlabel('Epoch')
    plt.ylabel('MSE Loss')
    plt.title(f'MSE Loss vs Epoch - Training.\n Train Loss: {min(losses):.4f} \n Test Loss: {test_loss.item():.4f}')
    plt.grid(True)
    plt.show()