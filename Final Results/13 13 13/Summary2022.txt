Cost Score: 0.3333333333333333  Sustainability Score: 0.3333333333333333  Reliability Score: 0.3333333333333333

Heuroistic policy changed the cumulative cost score by 0.5% in 92 days
Disounted FVI increased the cumulative cost score by 0.6% in 92 days

Heuroistic policy changed the cumulative reliability score by -19.4% in 92 days
Disounted FVI increased the reliability cost score by 3.8% in 92 days

However the Height of the well at the end of 92 days in Status Quo is: 155.26 inches
The Height of the well at the end of 92 days in Heuristic Policy is: 126.74 inches
The Height of the well at the end of 92 days in RL Optimized result is: 162.75 inches

Heuroistic policy changed the cumulative sustainability score by 0.6% in 92 days
Disounted FVI increased the cumulative sustainability score by 0.5% in 92 days

Heuristic Policy Results:
Total diesel electricity generation in 92 days changed from 3411.2609630003035 to 2870.63428300031 kWh which means -15.848294395058435% change.
Total waste energy in 92 days changed from 4139.888087132128 to 3780.2165616847637 kWh which means -8.687952859530645% change.
Minute-wise excess energy utilization changed from 22465 to 24731 mins: 10.086801691520142% change.

Discounted FVI Method Results:
Total diesel electricity generation in 92 days changed from 3411.2609630003035 to 3154.322886000299 kWh which means -7.532055735015354% change.
Total waste energy in 92 days changed from 4139.888087132128 to 2852.604153643098 kWh which means -31.09465537221285% change.
Minute-wise excess energy utilization changed from 22465 to 22701 mins: 1.050523035833519% change.

Average Cost Score:
SQ: 0.9646576227408439
Heuristic: 0.9690916930775082
RL Optimized: 0.9707263985932271

Average Sustainability Score:
SQ: 0.9519280635890853
Heuristic: 0.958092457093222
RL Optimized: 0.956795478646767

Average Reliability Score:
SQ: 0.5166387963804578
Heuristic: 0.4165640267280617
RL Optimized: 0.5361331833599601

Average Total Score:
SQ: 0.8110748275701289
Heuristic: 0.7812493922995971
RL Optimized: 0.821218353533318



RL: RO operates on average SOC of  94.01 %
RL: Well operates on average SOC of  93.8 %

SQ: RO operates on average SOC of  93.24 %
SQ: Well operates on average SOC of  91.25 %

HEU: RO operates on average SOC of  100.0 %
HEU: Well operates on average SOC of  100.0 %


