Cost Score: 0.5  Sustainability Score: 0.25  Reliability Score: 0.25

Heuroistic policy changed the cumulative cost score by 0.5% in 92 days
Disounted FVI increased the cumulative cost score by 0.6% in 92 days

Heuroistic policy changed the cumulative reliability score by -19.4% in 92 days
Disounted FVI increased the reliability cost score by 25.0% in 92 days

However the Height of the well at the end of 92 days in Status Quo is: 155.26 inches
The Height of the well at the end of 92 days in Heuristic Policy is: 126.74 inches
The Height of the well at the end of 92 days in RL Optimized result is: 195.97 inches

Heuroistic policy changed the cumulative sustainability score by 0.6% in 92 days
Disounted FVI increased the cumulative sustainability score by 0.3% in 92 days

Heuristic Policy Results:
Total diesel electricity generation in 92 days changed from 3411.2609630003035 to 2870.63428300031 kWh which means -15.848294395058435% change.
Total waste energy in 92 days changed from 4139.888087132128 to 3780.2165616847637 kWh which means -8.687952859530645% change.
Minute-wise excess energy utilization changed from 22465 to 24731 mins: 10.086801691520142% change.

Discounted FVI Method Results:
Total diesel electricity generation in 92 days changed from 3411.2609630003035 to 3455.052381000282 kWh which means 1.283731103394171% change.
Total waste energy in 92 days changed from 4139.888087132128 to 2317.5180838514334 kWh which means -44.01978906012229% change.
Minute-wise excess energy utilization changed from 22465 to 18946 mins: -15.664366792788783% change.

Average Cost Score:
SQ: 0.9646576227408439
Heuristic: 0.9690916930775082
RL Optimized: 0.9708370207838903

Average Sustainability Score:
SQ: 0.9519280635890853
Heuristic: 0.958092457093222
RL Optimized: 0.9545944164288417

Average Reliability Score:
SQ: 0.5166387963804578
Heuristic: 0.4165640267280617
RL Optimized: 0.6456490501630705

Average Total Score:
SQ: 0.8494705263628077
Heuristic: 0.828209967494075
RL Optimized: 0.8854793770399232



RL: RO operates on average SOC of  91.82 %
RL: Well operates on average SOC of  90.79 %

SQ: RO operates on average SOC of  93.24 %
SQ: Well operates on average SOC of  91.25 %

HEU: RO operates on average SOC of  100.0 %
HEU: Well operates on average SOC of  100.0 %


